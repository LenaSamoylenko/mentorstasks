﻿using System.Diagnostics;

namespace ImproveKnowledgeMutliThreading
{
    public static class PersonalTimer 
    {
        private static Stopwatch timer = null;

        public static double TimeSeconds { get => timer.ElapsedMilliseconds * 0.001; }
        public static int SleepTime { get; set; }

        public static bool StartTimer()
        {
            try
            {
                timer = new Stopwatch();
                timer.Start();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public static bool StopTimer()
        {
            try
            {
                timer.Stop();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}
