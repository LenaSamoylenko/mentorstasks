﻿using ImproveKnowledgeMutliThreading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.Tasks
{
    class CollectionRowsSymbolsCountTasks: CollectionRowsSymbolsCountParent
    {
        Task[] tasks;

        public CollectionRowsSymbolsCountTasks(IEnumerable<string> rowsCollection, int sleepTime=100) :base(sleepTime)
        {
            try
            {
                inCollectionCount = rowsCollection.Count();
                tasks = new Task[inCollectionCount];

                foreach (var row in rowsCollection)
                {
                    RowSymbolsCountTasks rowSymbolsCount = new RowSymbolsCountTasks(row, sleepTime);

                    sum += row.Length;

                    tasks[--inCollectionCount] = Task.Run(() =>
                    {
                        //Thread.Sleep(sleepTime);

                        if (resultCollection == null)
                        {
                            resultCollection = new Dictionary<char, int>(rowSymbolsCount.SymbolsCollection);
                        }
                        else
                        {
                            resultCollection.AddRowSymbolsCount(rowSymbolsCount.SymbolsCollection);
                        }

                    });

                    tasks[inCollectionCount].Wait();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                base.StopTimer();
            }
        }
    }
}
