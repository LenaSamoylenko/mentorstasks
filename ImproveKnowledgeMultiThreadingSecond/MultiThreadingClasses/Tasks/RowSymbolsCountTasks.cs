﻿using ImproveKnowledgeMutliThreading;
using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.Tasks
{
    class RowSymbolsCountTasks : RowSymbolsCountSync
    {
        private Task[] tasks;
        public RowSymbolsCountTasks(string stringRow, int sleepTime) : base(stringRow, sleepTime)
        {
            this.sleepTime = sleepTime;
        }

        protected override void GetAllRowsSymbolsCount(Dictionary<char, int> symbolsCollectionResult)
        {
            int i = 0;
            tasks = new Task[symbolsCollectionResult.Count];

            foreach (var symbol in symbolsCollectionResult.Keys.ToList())
            {
                tasks[i] = Task.Run(() =>
                {
                    var result = CountOfSymbol(symbol);
                    symbolsCollectionResult[result.Key] = result.Value;

                    i++;
                });


                tasks[i].Wait();
            }
        }

        protected override KeyValuePair<char, int> CountOfSymbol(char etalonSymbol)
        {
            int count = 0;
            List<char> optimizeSymbols = new List<char>();
            KeyValuePair<char, int> result;


            for (int i = 0; i < symbols.Count; i++)
            {
                lock (this)
                {
                    if (symbols[i] == etalonSymbol)
                    {
                        count++;
                    }
                    else
                    {
                        optimizeSymbols.Add(symbols[i]);
                    }
                }
            }

            symbols = optimizeSymbols;
            result = new KeyValuePair<char, int>(etalonSymbol, count);


            return result;
        }
    }
}
