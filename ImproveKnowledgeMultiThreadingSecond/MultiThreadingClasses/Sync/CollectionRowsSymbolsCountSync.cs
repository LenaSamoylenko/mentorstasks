﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ImproveKnowledgeMutliThreading.Sync
{
    public class CollectionRowsSymbolsCountSync : CollectionRowsSymbolsCountParent
    {
        public CollectionRowsSymbolsCountSync(IEnumerable<string> rowsCollection, int sleepTime=100) : base(sleepTime)
        {
            try
            {
                inCollectionCount = rowsCollection.Count();

                foreach (var row in rowsCollection)
                {
                    RowSymbolsCountSync rowSymbolsCount = new RowSymbolsCountSync(row, sleepTime);

                    //Thread.Sleep(sleepTime);

                    if (resultCollection == null)
                    {
                        resultCollection = new Dictionary<char, int>(rowSymbolsCount.SymbolsCollection);
                    }
                    else
                    {
                        resultCollection.AddRowSymbolsCount(rowSymbolsCount.SymbolsCollection);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                base.StopTimer();
            }
        }
    }
}

