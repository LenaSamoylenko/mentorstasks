﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ImproveKnowledgeMutliThreading.Sync
{
    public class RowSymbolsCountSync : IDisposable
    {
        protected Dictionary<char, int> symbolsCollectionResult;
        protected List<char> symbols;
        protected int sleepTime;

        public RowSymbolsCountSync(string stringRow, int sleepTime)
        {
            this.sleepTime = sleepTime;
            symbols = stringRow.ToCharArray().ToList();

            symbolsCollectionResult = new Dictionary<char, int>();

            for (int i = 0; i < ImplimentationRandomString.CountOfLetters; i++)
            {
                symbolsCollectionResult.Add(ImplimentationRandomString.GetSymbol(i), 0);
            }

            GetAllRowsSymbolsCount(symbolsCollectionResult);
        }

        protected virtual void GetAllRowsSymbolsCount(Dictionary<char, int> symbolsCollectionResult)
        {
            foreach (var symbol in symbolsCollectionResult.Keys.ToList())
            {
                var result = CountOfSymbol(symbol);
                symbolsCollectionResult[result.Key] = result.Value;
            }
        }

        protected virtual KeyValuePair<char, int> CountOfSymbol(char etalonSymbol)
        {
            int count = 0;
            List<char> optimizeSymbols = new List<char>();

            for (int i = 0; i < symbols.Count; i++)
            {
                if (symbols[i] == etalonSymbol)
                {
                    count++;
                }
                else
                {
                    optimizeSymbols.Add(symbols[i]);
                }
            }

            symbols = optimizeSymbols;

            return new KeyValuePair<char, int>(etalonSymbol, count);
        }


        public Dictionary<char, int> SymbolsCollection => symbolsCollectionResult;

        public int this[char index] => symbolsCollectionResult[index];

        public void Dispose()
        {
            GC.ReRegisterForFinalize(this);
            GC.Collect();
        }
    }
}

