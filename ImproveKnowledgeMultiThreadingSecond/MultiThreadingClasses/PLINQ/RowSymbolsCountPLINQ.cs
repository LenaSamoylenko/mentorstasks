﻿using ImproveKnowledgeMutliThreading;
using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.PLINQ
{
    class RowSymbolsCountPLINQ : RowSymbolsCountSync
    {
        public RowSymbolsCountPLINQ(string stringRow, int sleepTime) : base(stringRow, sleepTime)
        { }

        protected override void GetAllRowsSymbolsCount(Dictionary<char, int> symbolsCollectionResult)
        {
            (from symbol in symbolsCollectionResult.Keys.ToList().AsParallel()
             select CountOfSymbol(symbol))
                          .ForAll((result) =>
                                  {
                                      symbolsCollectionResult[result.Key] = result.Value;
                                  });
        }


        protected override KeyValuePair<char, int> CountOfSymbol(char etalonSymbol)
        {
            KeyValuePair<char, int> result;

            int count = 0;
            List<char> optimizeSymbols = new List<char>();

            lock (this)
            {
                for (int i = 0; i < symbols.Count; i++)
                {
                    if (symbols[i] == etalonSymbol)
                    {
                        count++;
                    }
                    else
                    {
                        optimizeSymbols.Add(symbols[i]);
                    }
                }

                result = new KeyValuePair<char, int>(etalonSymbol, count);
                symbols = optimizeSymbols;
            }

            return result;
        }
    }
}
