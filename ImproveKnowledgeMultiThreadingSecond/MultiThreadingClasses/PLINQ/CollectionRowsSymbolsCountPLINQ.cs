﻿using ImproveKnowledgeMutliThreading;
using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.PLINQ
{
    class CollectionRowsSymbolsCountPLINQ : CollectionRowsSymbolsCountParent
    {
        public CollectionRowsSymbolsCountPLINQ(IEnumerable<string> rowsCollection, int sleepTime = 100) : base(sleepTime)
        {
            try
            {
                inCollectionCount = rowsCollection.Count();

                rowsCollection.AsParallel().ForAll((row) =>
                            {
                                RowSymbolsCountPLINQ rowSymbolsCount = new RowSymbolsCountPLINQ(row, sleepTime);

                                if (resultCollection == null)
                                {
                                    resultCollection = new Dictionary<char, int>(rowSymbolsCount.SymbolsCollection);
                                }
                                else
                                {
                                    resultCollection.AddRowSymbolsCount(rowSymbolsCount.SymbolsCollection);
                                }
                            });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                base.StopTimer();
            }
        }

    }

}
