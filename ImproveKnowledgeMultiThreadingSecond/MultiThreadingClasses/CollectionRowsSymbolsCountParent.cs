﻿using System.Collections.Generic;

namespace ImproveKnowledgeMutliThreading
{
    public abstract class CollectionRowsSymbolsCountParent 
    {
        protected Dictionary<char, int> resultCollection = null;
        protected double time;
        protected int inCollectionCount;
        protected int sum;
        protected int sleepTime;

        public CollectionRowsSymbolsCountParent(int sleepTime)
        {
            this.sleepTime = sleepTime;
            PersonalTimer.StartTimer();
        }

        public string CommonResult()
        {
            return GetStringAnswer.GetSringResult(this.resultCollection, sum);
        }

        public string SpentTime()
        {
            return $"{time} seconds for {inCollectionCount} elements";
        }

        public void StopTimer() 
        {
            PersonalTimer.StopTimer();
            time = PersonalTimer.TimeSeconds;
        }
    }
}

