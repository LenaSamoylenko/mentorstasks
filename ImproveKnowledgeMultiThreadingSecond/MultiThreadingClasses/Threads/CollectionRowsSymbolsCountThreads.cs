﻿using ImproveKnowledgeMutliThreading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.Threads
{
    class CollectionRowsSymbolsCountTPL : CollectionRowsSymbolsCountParent
    {
        public CollectionRowsSymbolsCountTPL(IEnumerable<string> rowsCollection, int sleepTime = 100) : base(sleepTime)
        {
            try
            {
                inCollectionCount = rowsCollection.Count();

                foreach (var row in rowsCollection)
                {
                    RowSymbolsCountTPL rowSymbolsCount = new RowSymbolsCountTPL(row, sleepTime);
                    sum += row.Length;

                    Thread thread = new Thread(() =>
                         {
                             //Thread.Sleep(sleepTime);

                             if (resultCollection == null)
                             {
                                 resultCollection = new Dictionary<char, int>(rowSymbolsCount.SymbolsCollection);
                             }
                             else
                             {
                                 resultCollection.AddRowSymbolsCount(rowSymbolsCount.SymbolsCollection);
                             }

                         });

                    thread.Start();
                    thread.Join();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                base.StopTimer();
            }
        }

    }

}
