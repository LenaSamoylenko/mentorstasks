﻿using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.ConcurrentDictionary
{
    class RowSymbolsCountConcurrentDictionary : RowSymbolsCountSync
    {
        public RowSymbolsCountConcurrentDictionary(string stringRow, int sleepTime) : base(stringRow, sleepTime)
        { }

        protected void GetAllRowsSymbolsCount(ConcurrentDictionary<char, int> symbolsCollectionResult)
        {

            foreach (var symbol in symbolsCollectionResult.Keys.ToList())
            {
                var result = CountOfSymbol(symbol);
                symbolsCollectionResult[result.Key] = result.Value;
            }
        }


        protected override KeyValuePair<char, int> CountOfSymbol(char etalonSymbol)
        {
            KeyValuePair<char, int> result;

            int count = 0;
            List<char> optimizeSymbols = new List<char>();

            lock (this)
            {
                for (int i = 0; i < symbols.Count; i++)
                {
                    if (symbols[i] == etalonSymbol)
                    {
                        count++;
                    }
                    else
                    {
                        optimizeSymbols.Add(symbols[i]);
                    }
                }

                result = new KeyValuePair<char, int>(etalonSymbol, count);
                symbols = optimizeSymbols;
            }

            return result;
        }
    }
}
