﻿using ImproveKnowledgeMutliThreading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.ConcurrentDictionary
{
    class CollectionRowsSymbolsConcurrentDictionary : CollectionRowsSymbolsCountParent
    {
        public CollectionRowsSymbolsConcurrentDictionary(IEnumerable<string> rowsCollection, int sleepTime = 100) : base(sleepTime)
        {
            try
            {
                inCollectionCount = rowsCollection.Count();

                foreach (var row in rowsCollection)
                {
                    RowSymbolsCountConcurrentDictionary rowSymbolsCount = new RowSymbolsCountConcurrentDictionary(row, sleepTime);

                    //Thread.Sleep(sleepTime);

                    if (resultCollection == null)
                    {
                        resultCollection = new Dictionary<char, int>(rowSymbolsCount.SymbolsCollection);
                    }
                    else
                    {
                        resultCollection.AddRowSymbolsCount(rowSymbolsCount.SymbolsCollection);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                base.StopTimer();
            }
        }
    }
}
