﻿using ImproveKnowledgeMutliThreading;
using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.TPL
{
    class RowSymbolsCountTPL : RowSymbolsCountSync
    {
        public RowSymbolsCountTPL(string stringRow, int sleepTime) : base(stringRow, sleepTime)
        { }

        protected override void GetAllRowsSymbolsCount(Dictionary<char, int> symbolsCollectionResult)
        {
            Parallel.ForEach(symbolsCollectionResult.Keys.ToList(), symbol =>
                                                                    {
                                                                        var result = CountOfSymbol(symbol);
                                                                        symbolsCollectionResult[result.Key] = result.Value;
                                                                    });
        }

        protected override KeyValuePair<char, int> CountOfSymbol(char etalonSymbol)
        {
            KeyValuePair<char, int> result;

            int count = 0;
            List<char> optimizeSymbols = new List<char>();

            lock (this)
            {
                for (int i = 0; i < symbols.Count; i++)
                {
                    if (symbols[i] == etalonSymbol)
                    {
                        count++;
                    }
                    else
                    {
                        optimizeSymbols.Add(symbols[i]);
                    }
                }

                result = new KeyValuePair<char, int>(etalonSymbol, count);
                symbols = optimizeSymbols;
            }

            return result;
        }
    }
}
