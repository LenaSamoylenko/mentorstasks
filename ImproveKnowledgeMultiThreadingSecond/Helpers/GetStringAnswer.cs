﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImproveKnowledgeMutliThreading
{
    public static class GetStringAnswer
    {
        public static string GetSringResult(Dictionary<char, int> symbolsCollectionResult, int? sum=null)
        {
            StringBuilder resultRowBuilder = new StringBuilder();
            int summary = 0;

            foreach (var item in symbolsCollectionResult.Keys.ToList())
            {
                resultRowBuilder.Append("'");
                resultRowBuilder.Append(item);
                resultRowBuilder.Append("'");
                resultRowBuilder.Append(" : ");
                resultRowBuilder.Append(symbolsCollectionResult[item]);
                resultRowBuilder.Append("\t");

                summary += symbolsCollectionResult[item];
            }

            resultRowBuilder.Append("\nSummary: ");
            resultRowBuilder.Append(summary);

            //if (sum != 0)
            //{
            //    resultRowBuilder.Append("\nSummary from elements: ");
            //    resultRowBuilder.Append(sum);
            //}

            return resultRowBuilder.ToString();
        }
    }
}

