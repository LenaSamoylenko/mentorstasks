﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ImproveKnowledgeMutliThreading
{
    public static class DictionaryExtention
    {
        public static void AddRowSymbolsCount(this Dictionary<char, int> dictionary, Dictionary<char, int> collection)
        {
            try
            {
                foreach (var resultSymbol in dictionary.Keys.ToList())
                {
                    foreach (var item in collection)
                    {
                        lock ((object) item)
                        {
                            if (resultSymbol == item.Key)
                            {
                                dictionary[resultSymbol] += item.Value;
                                collection.Remove(item.Key);
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

