﻿using ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.ConcurrentDictionary;
using ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.PLINQ;
using ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.Tasks;
using ImproveKnowledgeMultiThreadingSecond.MultiThreadingClasses.Threads;
using ImproveKnowledgeMutliThreading;
using ImproveKnowledgeMutliThreading.Sync;
using System;
using System.Linq;

namespace ImproveKnowledgeMultiThreadingSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello!");

            //sleep time
            int sleepTime = 2; //milisecond

            //Get Collection of symbols rows
            GetCollectionOfRowsUsingTasks rowsCollection = new GetCollectionOfRowsUsingTasks();
            rowsCollection.FormCollection(100000, 1000);
            Console.WriteLine($"We`ve got collection of {rowsCollection.CollectionCount} elements");

            //Sync counter
            Console.WriteLine("\nSync counter");
            CollectionRowsSymbolsCountSync syncCollection = new CollectionRowsSymbolsCountSync(rowsCollection.GetCollection, sleepTime);
            Console.WriteLine(syncCollection.CommonResult());
            Console.WriteLine(syncCollection.SpentTime());

            ////Threads counter
            //Console.WriteLine("\nThreads counter");
            //CollectionRowsSymbolsCountThreads threadsCollection = new CollectionRowsSymbolsCountThreads(rowsCollection.GetCollection, sleepTime);
            //Console.WriteLine(threadsCollection.CommonResult());
            //Console.WriteLine(threadsCollection.SpentTime());

            //Tasks counter
            Console.WriteLine("\nTasks counter");
            CollectionRowsSymbolsCountTasks tasksCollection = new CollectionRowsSymbolsCountTasks(rowsCollection.GetCollection, sleepTime);
            Console.WriteLine(tasksCollection.CommonResult());
            Console.WriteLine(tasksCollection.SpentTime());

            //TPL counter
            Console.WriteLine("\nTPL counter");
            CollectionRowsSymbolsCountTPL tplCollection = new CollectionRowsSymbolsCountTPL(rowsCollection.GetCollection, sleepTime);
            Console.WriteLine(tplCollection.CommonResult());
            Console.WriteLine(tplCollection.SpentTime());

            //PLINQ counter
            Console.WriteLine("\nPLINQ counter");
            CollectionRowsSymbolsCountPLINQ plinqCollection = new CollectionRowsSymbolsCountPLINQ(rowsCollection.GetCollection, sleepTime);
            Console.WriteLine(plinqCollection.CommonResult());
            Console.WriteLine(plinqCollection.SpentTime());

            //ConcurrentDictionary counter
            Console.WriteLine("\nConcurrentDictionary counter");
            CollectionRowsSymbolsConcurrentDictionary concurrentDictionaryCollection = new CollectionRowsSymbolsConcurrentDictionary(rowsCollection.GetCollection, sleepTime);
            Console.WriteLine(concurrentDictionaryCollection.CommonResult());
            Console.WriteLine(concurrentDictionaryCollection.SpentTime());

            Console.ReadKey();

        }
    }


}
