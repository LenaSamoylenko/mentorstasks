﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMutliThreading
{
    public class ImplimentationRandomString
    {
        static char[] letters = null;
        private static Random random = null;
        private int symbolCount;

        static ImplimentationRandomString()
        {
            letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        }

        public ImplimentationRandomString(int count)
        {
            random = new Random();
            symbolCount = count;
        }

        public string GetRandowWord()
        {
            StringBuilder resultWord = new StringBuilder();

            for (int i = 0; i < symbolCount; i++)
            {
                char symbol = letters[random.Next(0, letters.Length)];

                resultWord.Append(symbol);
            }

            return resultWord.ToString();
        }

        public async Task<string> GetRandowWordAsync()
        {
            StringBuilder resultWord = new StringBuilder();
            Task[] tasks = new Task[symbolCount];


            for (int i = 0; i < symbolCount; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    char symbol = letters[random.Next(0, letters.Length - 1)];
                    resultWord.Append(symbol);
                });
            }

            await Task.WhenAll(tasks);

            return resultWord.ToString();
        }

        public static char GetSymbol(int index) => letters[index];

        public static int CountOfLetters { get => letters.Length; }

    }
}
