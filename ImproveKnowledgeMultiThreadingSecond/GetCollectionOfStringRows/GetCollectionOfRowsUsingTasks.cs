﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImproveKnowledgeMutliThreading
{
    public class GetCollectionOfRowsUsingTasks
    {
        private List<string> rowsCollection = null;

        public GetCollectionOfRowsUsingTasks()
        {
            rowsCollection = new List<string>();
        }

        public bool FormCollection(int elementsCount, int symbolCount)
        {
            ImplimentationRandomString elem = new ImplimentationRandomString(symbolCount);
            Task[] tasks = new Task[elementsCount];

            try
            {
                for (int i = 0; i < elementsCount; i++)
                {
                    tasks[i] = Task.Run(() =>
                    {
                        var word = elem.GetRandowWord();
                        rowsCollection.Add(word);
                    });

                    tasks[i].Wait();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool ClearCollection()
        {
            try
            {
                rowsCollection.Clear();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public string this[int index] => rowsCollection[index];
        public IEnumerable<string> GetCollection => rowsCollection;
        public int CollectionCount => rowsCollection.Count;
    }
}
