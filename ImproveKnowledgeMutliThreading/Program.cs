﻿using System;

namespace ImproveKnowledgeMutliThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");
            int firstCount = 100;
            int secondCount = 5000;
            int thirdCount = 50000;
            int fourthCount = 500000;

            string valueType = "sec";

            int symbolsCount = 1000;

            ////First
            Threads thr = new Threads();
            var timeThr1 = thr.GetCollectionSec(firstCount, symbolsCount);
            var timeThr2 = thr.GetCollectionSec(secondCount, symbolsCount);
            //var timeThr3 = thr.GetCollectionSec(thirdCount, symbolsCount);

            Console.WriteLine("Threads: ");
            Console.WriteLine(firstCount + ": " + timeThr1 + valueType);
            Console.WriteLine(secondCount + ": " + timeThr2 + valueType);
            //Console.WriteLine(thirdCount + ": "+timeThr3+ valueType);
            Console.WriteLine();
            thr.Dispose();

            ////Second
            Tasks tsk = new Tasks();
            var timeTsk1 = tsk.GetCollectionSec(firstCount, symbolsCount);
            var timeTsk2 = tsk.GetCollectionSec(secondCount, symbolsCount);
            var timeTsk3 = tsk.GetCollectionSec(thirdCount, symbolsCount);
            var timeTsk4 = tsk.GetCollectionSec(fourthCount, symbolsCount);

            Console.WriteLine("Tasks: ");
            Console.WriteLine(firstCount + ": " + timeTsk1 + valueType);
            Console.WriteLine(secondCount + ": " + timeTsk2 + valueType);
            Console.WriteLine(thirdCount + ": " + timeTsk3 + valueType);
            Console.WriteLine(fourthCount + ": " + timeTsk4 + valueType);

            Console.WriteLine();
            tsk.Dispose();

            //Third
            AsyncAwait asAw = new AsyncAwait();
            var timeasAw1 = asAw.GetCollectionSec(firstCount, symbolsCount);
            var timeasAw2 = asAw.GetCollectionSec(secondCount, symbolsCount);
            var timeasAw3 = asAw.GetCollectionSec(thirdCount, symbolsCount);
            var timeasAw4 = asAw.GetCollectionSec(fourthCount, symbolsCount);

            Console.WriteLine("Async/Await: ");
            Console.WriteLine(firstCount + ": " + timeasAw1 + valueType);
            Console.WriteLine(secondCount + ": " + timeasAw2 + valueType);
            Console.WriteLine(thirdCount + ": " + timeasAw3 + valueType);
            Console.WriteLine(thirdCount + ": " + timeasAw4 + valueType);

            Console.WriteLine();
            asAw.Dispose();

            Console.ReadKey();
        }
    }
}
