﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMutliThreading
{
    class Tasks : ParentGetCollection
    {
        public override double GetCollectionSec(int elementsCount, int symbolCount)
        {
            ImplimentationRandomString elem = new ImplimentationRandomString(symbolCount);
            Task[] tasks = new Task[elementsCount];

            try
            {
                StartTimer();

                for (int i = 0; i < elementsCount; i++)
                {
                    tasks[i] = Task.Run(() =>
                    {
                        Thread.Sleep(SleepTime);
                        var word = elem.GetRandowWord();
                        collection.Add(word);
                    });
                }

                Task.WaitAll(tasks);
            }
            finally
            {
                StopTimer();
                collection.Clear();
            }

            return TimeSeconds;
        }
    }
}
