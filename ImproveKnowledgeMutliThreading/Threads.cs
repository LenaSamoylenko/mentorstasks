﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ImproveKnowledgeMutliThreading
{
    class Threads : ParentGetCollection
    {
        public override double GetCollectionSec(int elementsCount, int symbolCount)
        {
            ImplimentationRandomString elem = new ImplimentationRandomString(symbolCount);

            try
            {
                StartTimer();

                for (int i = 0; i < elementsCount; i++)
                {
                    Thread.Sleep(SleepTime);
                    new Thread(() =>
                    {
                        var word = elem.GetRandowWord();
                        collection.Add(word);
                    }).Start();
                }
            }
            finally
            {
                StopTimer();
                collection.Clear();
            }

            return TimeSeconds;
        }
    }
}
