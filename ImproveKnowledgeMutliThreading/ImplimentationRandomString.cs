﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMutliThreading
{
    public class ImplimentationRandomString
    {
        char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        private Random random = null;
        private int symbolCount;

        public ImplimentationRandomString(int count)
        {
            random = new Random();
            symbolCount = count;
        }

        public string GetRandowWord()
        {
            StringBuilder resultWord = new StringBuilder();

            lock (this)
            {
                for (int i = 0; i < symbolCount; i++)
                {
                    char symbol = letters[random.Next(0, letters.Length - 1)];

                    resultWord.Append(symbol);
                }
            }

            return resultWord.ToString();
        }

        public async Task<string> GetRandowWordAsync()
        {
            StringBuilder resultWord = new StringBuilder();
            Task[] tasks = new Task[symbolCount];


            for (int i = 0; i < symbolCount; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    char symbol = letters[random.Next(0, letters.Length - 1)];
                    resultWord.Append(symbol);
                });
            }

            await Task.WhenAll(tasks);

            return resultWord.ToString();
        }

    }
}
