﻿using System.Threading;
using System.Threading.Tasks;

namespace ImproveKnowledgeMutliThreading
{
    class AsyncAwait : ParentGetCollection
    {
        public override double GetCollectionSec(int elementsCount, int symbolCount)
        {
            ImplimentationRandomString elem = new ImplimentationRandomString(symbolCount);
            Task[] tasks = new Task[elementsCount];

            try
            {
                StartTimer();

                var cl = GetCollectionAsync(elem, elementsCount, SleepTime);
            }
            finally
            {
                StopTimer();
                collection.Clear();
            }

            return TimeSeconds;
        }

        private static async Task<string[]> GetCollectionAsync(ImplimentationRandomString elem, int elementsCount, int sleepTime)
        {
            Task<string>[] tasks = new Task<string>[elementsCount];

            for (int i = 0; i < elementsCount; i++)
            {
                tasks[i] = Task.Run(async () =>
                {
                    Thread.Sleep(sleepTime);
                    var word = await elem.GetRandowWordAsync();
                    return word;
                });
            }

            var result =  await Task.WhenAll(tasks);

            return result;
        }
    }
}
