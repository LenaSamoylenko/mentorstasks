﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ImproveKnowledgeMutliThreading
{
    public abstract class ParentGetCollection : IDisposable
    {
        private Stopwatch timer = null;
        private int sleepTime = 10;
        protected List<string> collection = null;

        public ParentGetCollection()
        {
            collection = new List<string>();
        }

        public double TimeSeconds { get => timer.ElapsedMilliseconds * 0.001; }
        public int SleepTime { get; set; }

        public abstract double GetCollectionSec(int elementsCount, int symbolCount);

        public virtual bool StartTimer()
        {
            try
            {
                timer = new Stopwatch();
                timer.Start();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public virtual bool StopTimer()
        {
            try
            {
                timer.Stop();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public void Dispose()
        {
            GC.ReRegisterForFinalize(this);
            GC.Collect();
        }
    }
}
