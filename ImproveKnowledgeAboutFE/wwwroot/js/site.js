//Author class

class Author {
    constructor(fName, lName, birthYear, nickName) {
        this.fName = fName;
        this.lName = lName;
        this.birthYear = birthYear;
        this.nickName = nickName;
    }     
}

//Book class

class Book {
    constructor(name, publishYear, price) {
        this.name = name;
        this.publishYear = publishYear;
        this.price = price;
        this.authors = "";
    }
}

function createBooksTable(db) {
    // Create an objectStore for this database
    let objectStoreBooks = db.createObjectStore("Books", { keyPath: ["name", "authors"] });
    
    objectStoreBooks.createIndex("name", "name", { unique: false });
    objectStoreBooks.createIndex("publishYear", "publishYear", { unique: false });
    objectStoreBooks.createIndex("price", "price", { unique: false });
    objectStoreBooks.createIndex("authors", "authors", { unique: false });
};

function addNewBook(book) {
    let transaction = db.transaction(["Books"], "readwrite");
    let obStore = transaction.objectStore("Books");
    let request = obStore.add(book);

    request.onsuccess = function (event) {
        alert("Congratulation!! New book was added");
    };

    request.onerror = function (event) {
        alert("Such book is already exist");
    };
};

//create DB

// In the following line, you should include the prefixes of implementations you want to test.
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// DON'T use "var indexedDB = ..." if you're not in a function.
// Moreover, you may need references to some window.IDB* objects:
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || { READ_WRITE: "readwrite" }; // This line should only be needed if it is needed to support the object's constants for older browsers
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
window.IDBCursor = window.IDBCursor || window.IDBCursorWithValue;
window.IDBRequest = window.IDBOpenDBRequest;

if (!window.indexedDB) {
    console.log("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
}

//Open database
var dbReq = indexedDB.open('MyTestDatabase', 1);
var db;
var authors;

dbReq.onerror = function (event) {
    console.log("Some problems with DB(((");
};
dbReq.onsuccess = function (event) {
    db = event.target.result;
};

dbReq.onupgradeneeded = function (event) {
    db = event.target.result;

    //create Books table
    createBooksTable(db);

    //create Authors table
    createAuthorsTable(db);
};

function getAllAuthors() {
    const tx = db.transaction(['Authors'], 'readwrite'),
        store = tx.objectStore('Authors'),
        req = store.openCursor();

    store.add(new Author("", "", getDate(), "Inkognito"));

    authors = [];
    var elem = document.getElementById('btn_addNewAuthor');

    req.onsuccess = (event) => {
        // Результатом req.onsuccess в запросах openCursor является
        // IDBCursor
        let cursor = event.target.result;
        if (cursor != null) {
            // Если курсор не нулевой, мы получили элемент.
            authors.push(cursor.value);
            cursor.continue();
        }
        else {
            let listHTML = '<div id="authors_list">';
            for (let i = 0; i < authors.length; i++) {
                listHTML += '<div><button onclick="deleteNote(event)" ' + '">Delete</button>';
                listHTML += authors[i].nickName + ' ' + authors[i].fName + ' ' + authors[i].lName + '</div>';
            };
            listHTML += '</div>';

            document.body.innerHTML += listHTML;
        }
    }
    req.onerror = (event) => {
        alert('error in cursor request ' + event.target.errorCode);
    }
}

function getAllAuthorsForInputBook() {
    const tx = db.transaction(['Authors'], 'readwrite'),
        store = tx.objectStore('Authors'),
        req = store.openCursor();

    store.add(new Author("", "", getDate(), ""));

    authors = [];

    req.onsuccess = (event) => {
        // Результатом req.onsuccess в запросах openCursor является
        // IDBCursor
        let cursor = event.target.result;
        if (cursor != null) {
            // Если курсор не нулевой, мы получили элемент.
            authors.push(cursor.value);
            cursor.continue();
        }
        else {
            let elem = document.getElementById("inputSelectOptions" + inputVar);

            for (let i = 0; i < authors.length; i++) {
                let newOptItem = document.createElement("option");
                newOptItem.value = authors[i].nickName;
                newOptItem.text = authors[i].nickName;
                elem.appendChild(newOptItem);
            };
        }
    }
    req.onerror = (event) => {
        alert('error in cursor request ' + event.target.errorCode);
    }

    req.oncomplete = () => { return input; }
}

function createAuthorsTable(db) {
    // Create an objectStore for this database
    let objectStore = db.createObjectStore("Authors", { keyPath: "nickName" });

    // Create an index to search customers by name. We may have duplicates
    // so we can't use a unique index.
    objectStore.createIndex("fName", "fName", { unique: false });

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex("lName", "lName", { unique: false });

    objectStore.createIndex("birthYear", "birthYear", { unique: false });

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex("nickName", "nickName", { unique: false });
}

function addNewAuthor(author) {
    let transaction = db.transaction(["Authors"], "readwrite");
    let obStore = transaction.objectStore("Authors");
    let request = obStore.add(author);

    request.onerror = function (event) {
        alert("Some problem with adding new author");
    };
};

function addNewBook(book) {
    let transaction = db.transaction(["Books"], "readwrite");
    let obStore = transaction.objectStore("Books");
    let request = obStore.add(book);

    request.onerror = function (event) {
        alert("Some problem with adding new author");
    };
};
//view for Authors
$(document).ready(
    function () {
        let btn_add = document.createElement('input');
        btn_add.id = 'btn_addNewAuthor';
        btn_add.type = 'button';
        btn_add.value = 'Add new Author';
        btn_add.className = 'btn-primary';
        btn_add.setAttribute('onclick', 'getEvent(true);');

        let btn_remove = document.createElement(ipt);
        btn_remove.id = 'btn_removeAuthor';
        btn_remove.type = 'button';
        btn_remove.value = 'Clean Author';
        btn_remove.className = 'btn-primary';
        btn_remove.setAttribute('onclick', 'cleanInputForAuthor();');

        let btn_get = document.createElement(ipt);
        btn_get.id = 'btn_getAuthors';
        btn_get.type = 'button';
        btn_get.value = 'Get Authors';
        btn_get.className = 'btn-primary';
        btn_get.setAttribute('onclick', 'getAllAuthors();');

        document.body.appendChild(btn_add);
        document.body.appendChild(btn_remove);
        document.body.appendChild(btn_get);
    });

function addInputForAuthor() {
    let divId = 'newAuthorId';
    let lbl = 'label';
    let lbl_cl = "";
    let ipt = 'input';
    let ipt_cl = "";

    if (!document.getElementById(divId)) {
        let divAddNewAuthor = document.createElement('div');
        divAddNewAuthor.id = divId;
        divAddNewAuthor.className = "";

        //fName
        let lbl_input1 = document.createElement(lbl);
        lbl_input1.type = lbl;
        lbl_input1.textContent = "First Name";
        lbl_input1.className = lbl_cl;

        let input1 = document.createElement(ipt);
        input1.id = "input1_newAuthor";
        input1.type = ipt;
        input1.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input1);
        divAddNewAuthor.appendChild(input1);

        //lName
        let lbl_input2 = document.createElement(lbl);
        lbl_input2.type = lbl;
        lbl_input2.textContent = "Second Name";
        lbl_input2.className = lbl_cl;

        let input2 = document.createElement(ipt);
        input2.id = "input2_newAuthor";
        input2.type = ipt;
        input2.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input2);
        divAddNewAuthor.appendChild(input2);

        //birthYear;
        let lbl_input3 = document.createElement(lbl);
        lbl_input3.type = lbl;
        lbl_input3.textContent = "Year of birth";
        lbl_input3.className = lbl_cl;

        let input3 = document.createElement(ipt);
        input3.id = "input3_newAuthor";
        input3.type = 'date';
        let data = getDate().toString();
        input3.value = data;
        input3.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input3);
        divAddNewAuthor.appendChild(input3);

        //nickName;
        let lbl_input4 = document.createElement(lbl);
        lbl_input4.type = lbl;
        lbl_input4.textContent = "Nick Name";
        lbl_input4.className = lbl_cl;

        let input4 = document.createElement(ipt);
        input4.id = "input4_newAuthor";
        input4.type = ipt;
        input4.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input4);
        divAddNewAuthor.appendChild(input4);

        //final build
        var elem = document.getElementById('btn_addNewAuthor');
        elem.setAttribute("onclick", "getEvent(false)");
        elem.parentNode.insertBefore(divAddNewAuthor, elem);
    }
}

function getDate() {
    let todayDate = new Date();
    let currYear = todayDate.getFullYear().toString();
    let currMonth = (todayDate.getMonth() + 1).toString();
    let currDay = todayDate.getDate().toString();


    if (currYear.length < 4) {
        currYear = currYear.padStart(4, 0);
    }
    else if (currMonth.length < 2) {
        currMonth = currMonth.padStart(2, 0);
    }
    else if (currDay.length < 2) {
        currDay = currDay.padStart(2, 0);
    }

    var result = currYear + "-" + currMonth + "-" + currDay;
    return result;
}

function addNewAuthorToLibrary() {
    let container = document.getElementById("newAuthorId");
    let value = container.getElementsByTagName('input');

    if ((value[0].value && value[1].value && value[2].value && value[3].value) || value.length > 4) {
        addNewAuthor(new Author(value[0].value, value[1].value, value[2].value, value[3].value));

        container.remove();
        document.getElementById("btn_addNewAuthor").setAttribute('onclick', 'getEvent(true);');
        alert("The new Author was successfuly added");
    }
    else {
        alert("Some field is empty");
    }
}

function getEvent(eventType) {
    if (eventType) {
        return addInputForAuthor();
    }
    else {
        return addNewAuthorToLibrary();
    }
}

function cleanInputForAuthor() {
    let divAddNewAuthor = document.getElementById("newAuthorId");
    let divAuthorsList = document.getElementById("authors_list");

    if (divAddNewAuthor != undefined) {
        divAddNewAuthor.remove();
    }
    if (divAuthorsList != undefined) {
        divAuthorsList.remove();
    }
}

//view for Books
$(document).ready(
    function () {
        let btn_add = document.createElement(ipt);
        btn_add.id = 'btn_addNewBook';
        btn_add.type = 'button';
        btn_add.value = 'Add new Book';
        btn_add.className = 'btn-primary';
        btn_add.setAttribute('onclick', 'addInputForBook();')

        let btn_remove = document.createElement(ipt);
        btn_remove.id = 'btn_removeBook';
        btn_remove.type = 'button';
        btn_remove.value = 'Clean Book';
        btn_remove.className = 'btn-primary';
        btn_remove.setAttribute('onclick', 'cleanInputForBook();')

        let btn_get = document.createElement(ipt);
        btn_get.id = 'btn_getBooks';
        btn_get.type = 'button';
        btn_get.value = 'Get All Books';
        btn_get.className = 'btn-primary';
        btn_get.setAttribute('onclick', '')

        document.body.appendChild(btn_add);
        document.body.appendChild(btn_remove);
        document.body.appendChild(btn_get);
    }
);

var divId = 'newBookId';
var lbl = 'label';
var lbl_cl = "library-label";
var ipt = 'input';
var ipt_cl = "library-input";
var inputVar = 1;

function addInputForBook() {
    inputVar = 3;

    if (!document.getElementById(divId)) {
        let divAddNewBook = document.createElement('div');
        divAddNewBook.id = divId;
        divAddNewBook.className = "";

        //name
        let lbl_input1 = document.createElement(lbl);
        lbl_input1.type = lbl;
        lbl_input1.textContent = "Book`s Name";
        lbl_input1.className = lbl_cl;

        let input1 = document.createElement(ipt);
        input1.id = "input1_newBook";
        input1.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input1);
        divAddNewBook.appendChild(input1);

        //publishYear
        let lbl_input2 = document.createElement(lbl);
        lbl_input2.type = lbl;
        lbl_input2.textContent = "Publish Year";
        lbl_input2.className = lbl_cl;

        let input2 = document.createElement(ipt);
        input2.id = "input2_newBook";
        input2.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input2);
        divAddNewBook.appendChild(input2);

        //price
        let lbl_input3 = document.createElement(lbl);
        lbl_input3.type = lbl;
        lbl_input3.textContent = "Price";
        lbl_input3.className = lbl_cl;

        let input3 = document.createElement(ipt);
        input3.id = "input3_newBook";
        //input3.type = "";
        input3.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input3);
        divAddNewBook.appendChild(input3);

        //add label, input, btn
        addRepeaterComponents(divAddNewBook);

        //final build
        var elem = document.getElementById('btn_addNewBook');
        elem.setAttribute("onclick", "getEventBook()");
        elem.parentNode.insertBefore(divAddNewBook, elem);

        getAllAuthorsForInputBook();
    }
    else {
        alert("Sorry, you couldn`t add another input for new book");
    }
}

function cleanInputForBook() {
    let divAddNewBook = document.getElementById(divId);

    divAddNewBook.remove();
}

function addRepeaterComponents(divAddNewBook) {
    let c = divAddNewBook.getElementsByTagName(ipt);

    for (variable in c) {
        if (variable.textContent == undefined && inputVar != 3) {
            alert("Sorry, fill in previous cells");
            return;
        }
    }

    inputVar++;

    let label = addLabelForNewAuthors();
    divAddNewBook.appendChild(label);

    let input = addInputForNewAuthors();
    divAddNewBook.appendChild(input);

    let button = addButtonForNewAuthors();
    button.onclick = function () {
        addRepeaterComponents(divAddNewBook);
    };
    divAddNewBook.appendChild(button);

}

function addLabelForNewAuthors() {
    let label = document.createElement(lbl);
    label.type = lbl;
    label.textContent = "Nick Name";
    label.className = lbl_cl;

    return label;
}

function addInputForNewAuthors() {
    let name = ipt + "SelectOptions" + inputVar;
    let input = document.createElement("select");
    input.id = name;

    return input;
}

function addButtonForNewAuthors() {
    //button to add new Author
    let btn = document.createElement(ipt);
    btn.id = 'btn_addNewAuthorForThisBook' + inputVar;
    btn.type = 'button';
    btn.value = 'Add new Author for this Book';

    return btn;
}

//function getPrice() {
//    let todayDate = new Date();
//    let currYear = todayDate.getFullYear().toString();
//    let currMonth = todayDate.getMonth().toString();
//    let currDay = todayDate.getDate().toString();


//    if (currYear.length < 4) {
//        currYear = currYear.padStart(4, 0);
//    }
//    else if (currMonth.length < 2) {
//        currMonth = currMonth.padStart(2, 0);
//    }
//    else if (currDay.length < 2) {
//        currDay = currDay.padStart(2, 0);
//    }

//    return (currYear + "-" + currMonth + "-" + currDay);
//}

function addNewBookToLibrary() {
    let container = document.getElementById(divId);
    let valueInput = container.getElementsByTagName(ipt);
    let inputCount = valueInput.length;

    if (valueInput[inputCount - 1] == null || valueInput[inputCount - 1] == undefined) {
        valueInput.pop();
        inputCount = valueInput.length;
    }

    if ((valueInput[0].value && valueInput[1].value && valueInput[2].value)) {
        let newBook = new Author(valueInput[0].value, valueInput[1].value, valueInput[2].value);
        
        debugger
        valueInput.splice(0,3);

        for (item in valueInput) {
            newBook.authors.push(item.value);
        }

        addNewBook(newBook);
        
    }
    else {
        alert("Some field is empty");
    }
}

function getEventBook(eventType) {
    if (eventType) {
        alert("if block")
        addNewBookToLibrary();
    }
    else {
        addInputForBook();
    }
}