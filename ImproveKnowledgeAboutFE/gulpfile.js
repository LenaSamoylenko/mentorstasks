/// <binding BeforeBuild='min, less, js' ProjectOpened='less' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify-es").default,
    less = require("gulp-less");

var paths = {
    webroot: "./Content/"
};

paths.js = paths.webroot + "js/*.js";
paths.minJs = paths.webroot + "js/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";

paths.concatCssDest = "wwwroot/" + "css/site.min.css";
paths.concatJsDest = "wwwroot/js/site.js";
paths.concatJsDestMin = "wwwroot/js/site.min.js";

gulp.task('less', function () {
    gulp.src('./Content/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('./wwwroot/' + 'css'));
});

gulp.task("max:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(gulp.dest("."));
});

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDestMin))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});


gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});


gulp.task("js", gulp.series(["max:js", "min:js"]));
gulp.task("min", gulp.series(["min:js", "min:css"]));
