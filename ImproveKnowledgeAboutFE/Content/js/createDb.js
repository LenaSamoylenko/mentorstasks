﻿//create DB

// In the following line, you should include the prefixes of implementations you want to test.
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// DON'T use "var indexedDB = ..." if you're not in a function.
// Moreover, you may need references to some window.IDB* objects:
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || { READ_WRITE: "readwrite" }; // This line should only be needed if it is needed to support the object's constants for older browsers
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
window.IDBCursor = window.IDBCursor || window.IDBCursorWithValue;
window.IDBRequest = window.IDBOpenDBRequest;

if (!window.indexedDB) {
    console.log("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
}

//Open database
var dbReq = indexedDB.open('MyTestDatabase', 1);
var db;
var authors;

dbReq.onerror = function (event) {
    console.log("Some problems with DB(((");
};
dbReq.onsuccess = function (event) {
    db = event.target.result;
};

dbReq.onupgradeneeded = function (event) {
    db = event.target.result;

    //create Books table
    createBooksTable(db);

    //create Authors table
    createAuthorsTable(db);
};

function getAllAuthors() {
    const tx = db.transaction(['Authors'], 'readwrite'),
        store = tx.objectStore('Authors'),
        req = store.openCursor();

    store.add(new Author("", "", getDate(), "Inkognito"));

    authors = [];
    var elem = document.getElementById('btn_addNewAuthor');

    req.onsuccess = (event) => {
        // Результатом req.onsuccess в запросах openCursor является
        // IDBCursor
        let cursor = event.target.result;
        if (cursor != null) {
            // Если курсор не нулевой, мы получили элемент.
            authors.push(cursor.value);
            cursor.continue();
        }
        else {
            let listHTML = '<div id="authors_list">';
            for (let i = 0; i < authors.length; i++) {
                listHTML += '<div><button onclick="deleteNote(event)" ' + '">Delete</button>';
                listHTML += authors[i].nickName + ' ' + authors[i].fName + ' ' + authors[i].lName + '</div>';
            };
            listHTML += '</div>';

            document.body.innerHTML += listHTML;
        }
    }
    req.onerror = (event) => {
        alert('error in cursor request ' + event.target.errorCode);
    }
}

function getAllAuthorsForInputBook() {
    const tx = db.transaction(['Authors'], 'readwrite'),
        store = tx.objectStore('Authors'),
        req = store.openCursor();

    store.add(new Author("", "", getDate(), ""));

    authors = [];

    req.onsuccess = (event) => {
        // Результатом req.onsuccess в запросах openCursor является
        // IDBCursor
        let cursor = event.target.result;
        if (cursor != null) {
            // Если курсор не нулевой, мы получили элемент.
            authors.push(cursor.value);
            cursor.continue();
        }
        else {
            let elem = document.getElementById("inputSelectOptions" + inputVar);

            for (let i = 0; i < authors.length; i++) {
                let newOptItem = document.createElement("option");
                newOptItem.value = authors[i].nickName;
                newOptItem.text = authors[i].nickName;
                elem.appendChild(newOptItem);
            };
        }
    }
    req.onerror = (event) => {
        alert('error in cursor request ' + event.target.errorCode);
    }

    req.oncomplete = () => { return input; }
}

function createAuthorsTable(db) {
    // Create an objectStore for this database
    let objectStore = db.createObjectStore("Authors", { keyPath: "nickName" });

    // Create an index to search customers by name. We may have duplicates
    // so we can't use a unique index.
    objectStore.createIndex("fName", "fName", { unique: false });

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex("lName", "lName", { unique: false });

    objectStore.createIndex("birthYear", "birthYear", { unique: false });

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex("nickName", "nickName", { unique: false });
}

function addNewAuthor(author) {
    let transaction = db.transaction(["Authors"], "readwrite");
    let obStore = transaction.objectStore("Authors");
    let request = obStore.add(author);

    request.onerror = function (event) {
        alert("Some problem with adding new author");
    };
};

function addNewBook(book) {
    let transaction = db.transaction(["Books"], "readwrite");
    let obStore = transaction.objectStore("Books");
    let request = obStore.add(book);

    request.onerror = function (event) {
        alert("Some problem with adding new author");
    };
};