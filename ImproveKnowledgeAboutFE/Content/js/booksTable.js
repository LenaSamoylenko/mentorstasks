﻿function createBooksTable(db) {
    // Create an objectStore for this database
    let objectStoreBooks = db.createObjectStore("Books", { keyPath: ["name", "authors"] });
    
    objectStoreBooks.createIndex("name", "name", { unique: false });
    objectStoreBooks.createIndex("publishYear", "publishYear", { unique: false });
    objectStoreBooks.createIndex("price", "price", { unique: false });
    objectStoreBooks.createIndex("authors", "authors", { unique: false });
};

function addNewBook(book) {
    let transaction = db.transaction(["Books"], "readwrite");
    let obStore = transaction.objectStore("Books");
    let request = obStore.add(book);

    request.onsuccess = function (event) {
        alert("Congratulation!! New book was added");
    };

    request.onerror = function (event) {
        alert("Such book is already exist");
    };
};
