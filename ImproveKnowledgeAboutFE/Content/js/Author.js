﻿//Author class

class Author {
    constructor(fName, lName, birthYear, nickName) {
        this.fName = fName;
        this.lName = lName;
        this.birthYear = birthYear;
        this.nickName = nickName;
    }     
}
