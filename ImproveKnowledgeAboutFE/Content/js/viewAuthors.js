﻿//view for Authors
$(document).ready(
    function () {
        let btn_add = document.createElement('input');
        btn_add.id = 'btn_addNewAuthor';
        btn_add.type = 'button';
        btn_add.value = 'Add new Author';
        btn_add.className = 'btn-primary';
        btn_add.setAttribute('onclick', 'getEvent(true);');

        let btn_remove = document.createElement(ipt);
        btn_remove.id = 'btn_removeAuthor';
        btn_remove.type = 'button';
        btn_remove.value = 'Clean Author';
        btn_remove.className = 'btn-primary';
        btn_remove.setAttribute('onclick', 'cleanInputForAuthor();');

        let btn_get = document.createElement(ipt);
        btn_get.id = 'btn_getAuthors';
        btn_get.type = 'button';
        btn_get.value = 'Get Authors';
        btn_get.className = 'btn-primary';
        btn_get.setAttribute('onclick', 'getAllAuthors();');

        document.body.appendChild(btn_add);
        document.body.appendChild(btn_remove);
        document.body.appendChild(btn_get);
    });

function addInputForAuthor() {
    let divId = 'newAuthorId';
    let lbl = 'label';
    let lbl_cl = "";
    let ipt = 'input';
    let ipt_cl = "";

    if (!document.getElementById(divId)) {
        let divAddNewAuthor = document.createElement('div');
        divAddNewAuthor.id = divId;
        divAddNewAuthor.className = "";

        //fName
        let lbl_input1 = document.createElement(lbl);
        lbl_input1.type = lbl;
        lbl_input1.textContent = "First Name";
        lbl_input1.className = lbl_cl;

        let input1 = document.createElement(ipt);
        input1.id = "input1_newAuthor";
        input1.type = ipt;
        input1.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input1);
        divAddNewAuthor.appendChild(input1);

        //lName
        let lbl_input2 = document.createElement(lbl);
        lbl_input2.type = lbl;
        lbl_input2.textContent = "Second Name";
        lbl_input2.className = lbl_cl;

        let input2 = document.createElement(ipt);
        input2.id = "input2_newAuthor";
        input2.type = ipt;
        input2.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input2);
        divAddNewAuthor.appendChild(input2);

        //birthYear;
        let lbl_input3 = document.createElement(lbl);
        lbl_input3.type = lbl;
        lbl_input3.textContent = "Year of birth";
        lbl_input3.className = lbl_cl;

        let input3 = document.createElement(ipt);
        input3.id = "input3_newAuthor";
        input3.type = 'date';
        let data = getDate().toString();
        input3.value = data;
        input3.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input3);
        divAddNewAuthor.appendChild(input3);

        //nickName;
        let lbl_input4 = document.createElement(lbl);
        lbl_input4.type = lbl;
        lbl_input4.textContent = "Nick Name";
        lbl_input4.className = lbl_cl;

        let input4 = document.createElement(ipt);
        input4.id = "input4_newAuthor";
        input4.type = ipt;
        input4.className = ipt_cl;

        divAddNewAuthor.appendChild(lbl_input4);
        divAddNewAuthor.appendChild(input4);

        //final build
        var elem = document.getElementById('btn_addNewAuthor');
        elem.setAttribute("onclick", "getEvent(false)");
        elem.parentNode.insertBefore(divAddNewAuthor, elem);
    }
}

function getDate() {
    let todayDate = new Date();
    let currYear = todayDate.getFullYear().toString();
    let currMonth = (todayDate.getMonth() + 1).toString();
    let currDay = todayDate.getDate().toString();


    if (currYear.length < 4) {
        currYear = currYear.padStart(4, 0);
    }
    else if (currMonth.length < 2) {
        currMonth = currMonth.padStart(2, 0);
    }
    else if (currDay.length < 2) {
        currDay = currDay.padStart(2, 0);
    }

    var result = currYear + "-" + currMonth + "-" + currDay;
    return result;
}

function addNewAuthorToLibrary() {
    let container = document.getElementById("newAuthorId");
    let value = container.getElementsByTagName('input');

    if ((value[0].value && value[1].value && value[2].value && value[3].value) || value.length > 4) {
        addNewAuthor(new Author(value[0].value, value[1].value, value[2].value, value[3].value));

        container.remove();
        document.getElementById("btn_addNewAuthor").setAttribute('onclick', 'getEvent(true);');
        alert("The new Author was successfuly added");
    }
    else {
        alert("Some field is empty");
    }
}

function getEvent(eventType) {
    if (eventType) {
        return addInputForAuthor();
    }
    else {
        return addNewAuthorToLibrary();
    }
}

function cleanInputForAuthor() {
    let divAddNewAuthor = document.getElementById("newAuthorId");
    let divAuthorsList = document.getElementById("authors_list");

    if (divAddNewAuthor != undefined) {
        divAddNewAuthor.remove();
    }
    if (divAuthorsList != undefined) {
        divAuthorsList.remove();
    }
}
