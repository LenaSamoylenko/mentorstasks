﻿//view for Books
$(document).ready(
    function () {
        let btn_add = document.createElement(ipt);
        btn_add.id = 'btn_addNewBook';
        btn_add.type = 'button';
        btn_add.value = 'Add new Book';
        btn_add.className = 'btn-primary';
        btn_add.setAttribute('onclick', 'addInputForBook();')

        let btn_remove = document.createElement(ipt);
        btn_remove.id = 'btn_removeBook';
        btn_remove.type = 'button';
        btn_remove.value = 'Clean Book';
        btn_remove.className = 'btn-primary';
        btn_remove.setAttribute('onclick', 'cleanInputForBook();')

        let btn_get = document.createElement(ipt);
        btn_get.id = 'btn_getBooks';
        btn_get.type = 'button';
        btn_get.value = 'Get All Books';
        btn_get.className = 'btn-primary';
        btn_get.setAttribute('onclick', '')

        document.body.appendChild(btn_add);
        document.body.appendChild(btn_remove);
        document.body.appendChild(btn_get);
    }
);

var divId = 'newBookId';
var lbl = 'label';
var lbl_cl = "library-label";
var ipt = 'input';
var ipt_cl = "library-input";
var inputVar = 1;

function addInputForBook() {
    inputVar = 3;

    if (!document.getElementById(divId)) {
        let divAddNewBook = document.createElement('div');
        divAddNewBook.id = divId;
        divAddNewBook.className = "";

        //name
        let lbl_input1 = document.createElement(lbl);
        lbl_input1.type = lbl;
        lbl_input1.textContent = "Book`s Name";
        lbl_input1.className = lbl_cl;

        let input1 = document.createElement(ipt);
        input1.id = "input1_newBook";
        input1.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input1);
        divAddNewBook.appendChild(input1);

        //publishYear
        let lbl_input2 = document.createElement(lbl);
        lbl_input2.type = lbl;
        lbl_input2.textContent = "Publish Year";
        lbl_input2.className = lbl_cl;

        let input2 = document.createElement(ipt);
        input2.id = "input2_newBook";
        input2.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input2);
        divAddNewBook.appendChild(input2);

        //price
        let lbl_input3 = document.createElement(lbl);
        lbl_input3.type = lbl;
        lbl_input3.textContent = "Price";
        lbl_input3.className = lbl_cl;

        let input3 = document.createElement(ipt);
        input3.id = "input3_newBook";
        //input3.type = "";
        input3.className = ipt_cl;

        divAddNewBook.appendChild(lbl_input3);
        divAddNewBook.appendChild(input3);

        //add label, input, btn
        addRepeaterComponents(divAddNewBook);

        //final build
        var elem = document.getElementById('btn_addNewBook');
        elem.setAttribute("onclick", "getEventBook()");
        elem.parentNode.insertBefore(divAddNewBook, elem);

        getAllAuthorsForInputBook();
    }
    else {
        alert("Sorry, you couldn`t add another input for new book");
    }
}

function cleanInputForBook() {
    let divAddNewBook = document.getElementById(divId);

    divAddNewBook.remove();
}

function addRepeaterComponents(divAddNewBook) {
    let c = divAddNewBook.getElementsByTagName(ipt);

    for (variable in c) {
        if (variable.textContent == undefined && inputVar != 3) {
            alert("Sorry, fill in previous cells");
            return;
        }
    }

    inputVar++;

    let label = addLabelForNewAuthors();
    divAddNewBook.appendChild(label);

    let input = addInputForNewAuthors();
    divAddNewBook.appendChild(input);

    let button = addButtonForNewAuthors();
    button.onclick = function () {
        addRepeaterComponents(divAddNewBook);
    };
    divAddNewBook.appendChild(button);

}

function addLabelForNewAuthors() {
    let label = document.createElement(lbl);
    label.type = lbl;
    label.textContent = "Nick Name";
    label.className = lbl_cl;

    return label;
}

function addInputForNewAuthors() {
    let name = ipt + "SelectOptions" + inputVar;
    let input = document.createElement("select");
    input.id = name;

    return input;
}

function addButtonForNewAuthors() {
    //button to add new Author
    let btn = document.createElement(ipt);
    btn.id = 'btn_addNewAuthorForThisBook' + inputVar;
    btn.type = 'button';
    btn.value = 'Add new Author for this Book';

    return btn;
}

//function getPrice() {
//    let todayDate = new Date();
//    let currYear = todayDate.getFullYear().toString();
//    let currMonth = todayDate.getMonth().toString();
//    let currDay = todayDate.getDate().toString();


//    if (currYear.length < 4) {
//        currYear = currYear.padStart(4, 0);
//    }
//    else if (currMonth.length < 2) {
//        currMonth = currMonth.padStart(2, 0);
//    }
//    else if (currDay.length < 2) {
//        currDay = currDay.padStart(2, 0);
//    }

//    return (currYear + "-" + currMonth + "-" + currDay);
//}

function addNewBookToLibrary() {
    let container = document.getElementById(divId);
    let valueInput = container.getElementsByTagName(ipt);
    let inputCount = valueInput.length;

    if (valueInput[inputCount - 1] == null || valueInput[inputCount - 1] == undefined) {
        valueInput.pop();
        inputCount = valueInput.length;
    }

    if ((valueInput[0].value && valueInput[1].value && valueInput[2].value)) {
        let newBook = new Author(valueInput[0].value, valueInput[1].value, valueInput[2].value);
        
        debugger
        valueInput.splice(0,3);

        for (item in valueInput) {
            newBook.authors.push(item.value);
        }

        addNewBook(newBook);
        
    }
    else {
        alert("Some field is empty");
    }
}

function getEventBook(eventType) {
    if (eventType) {
        alert("if block")
        addNewBookToLibrary();
    }
    else {
        addInputForBook();
    }
}